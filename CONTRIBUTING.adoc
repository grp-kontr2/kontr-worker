= How to contribute to the Kontr Worker

Thank you for considering contributing to Kontr Worker!


== General Contribution Guide

- Take a look here: link:https://gitlab.fi.muni.cz/grp-kontr2/kontr-documentation/blob/master/contributing/GeneralContributionGuide.adoc[General Contribution Guide]