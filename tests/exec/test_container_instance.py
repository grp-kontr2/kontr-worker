import uuid

import mock
import pytest
import pytest_mock
from docker.models.containers import ContainerCollection
from flask import Flask
from mockredis import mock_redis_client

from tests.global_stubs import NetworkStub
from kontr_worker.entities import Submission
from kontr_worker.execution.containers import Instance
from kontr_worker.extensions import context


@pytest.fixture()
def config() -> dict:
    return dict(
        id=str(uuid.uuid4()),
        project_id='foo',
        course_id='bar',
        test_files_hash='for-bar-hash',
    )


@pytest.fixture()
def submission_instance(config) -> Submission:
    return Submission(config)


@pytest.fixture()
def instance(app: Flask, submission_instance) -> Instance:
    return context.docker.get_instance(submission_instance)


def test_instance_is_correctly_created(instance: Instance, submission_instance):
    assert instance.submission == submission_instance
    assert instance.wrapper == context.docker


@mock.patch('redis.Redis', mock_redis_client)
@pytest.mark.docker
def test_instance_is_runnable_correctly(instance: Instance,
                                        mocker: pytest_mock.MockFixture):
    with mocker.mock_module.patch.object(ContainerCollection, 'create',
                                         return_value=True, autospec=True):
        instance.hashes.set(instance.submission.unique_project_id, 'test-image')
        instance.create()
        assert ContainerCollection.create.call_count == 1
        args = ContainerCollection.create.call_args[1]
        submission_id = instance.submission.id
        assert args['image'] == 'test-image'
        assert args['name'] == f'kontr-submission-{submission_id}'
        assert args['volumes'] is not None

