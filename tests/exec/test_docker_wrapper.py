from pathlib import Path

import pytest
from flask import Flask

from kontr_worker.entities import Submission
from kontr_worker.extensions import context


@pytest.fixture()
def test_submission() -> Submission:
    return Submission({
        'id': 0
    })


def test_docker_wrapper_app_has_been_initialized(app):
    assert context.docker.app is not None


@pytest.mark.docker
def test_docker_wrapper_has_initialized_docker(app):
    assert context.docker.docker is not None


def test_docker_wrapper_has_correct_workspace_dir(app: Flask):
    assert context.docker.workspace_dir == Path(app.config['WORKSPACE_DIR'])


def test_docker_wrapper_has_correct_mount_dir(app: Flask):
    assert context.docker.mount_dir == Path(app.config['MOUNT_DIR'])


class NetworkStub(object):
    def __init__(self, *_):
        pass

    @property
    def name(self):
        return 'internal-network'

@pytest.mark.docker
def test_docker_wrapper_can_create_instance(app: Flask, test_submission: Submission):
    instance = context.docker.get_instance(test_submission)
    assert instance is not None
    assert instance.wrapper == context.docker
    assert instance.container is None
    assert instance.submission == test_submission
