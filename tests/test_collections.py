from flask import Flask
import json
from mockredis import mock_redis_client
import pytest
import mock

from kontr_worker.collections import RedisAbstractCollection


class StubEntity(object):
    def __init__(self, id=0):
        self.id = 'test_entity_' + str(id)
        self.config = {'key': 'value' + str(id), 'id': self.id}

    def __eq__(self, other):
        return self.id == other.id and self.config == other.flask_config


@pytest.fixture()
def collection(app):
    return RedisAbstractCollection('test_collection')


@pytest.fixture()
def entity_stub():
    return StubEntity()


@pytest.fixture()
def entity_stub_list():
    return [StubEntity(i) for i in range(10)]


def test_redis_collections_init(collection):
    assert collection.entity_klass_instance('{}') == {}
    assert collection.collection_name == 'test_collection'


@mock.patch('redis.Redis', mock_redis_client)
def test_redis_collections_get(collection: RedisAbstractCollection, app: Flask):
    redis = collection.redis
    redis.hset('test_collection', 'test_key', json.dumps({"test": 123}))
    assert {'test': 123} == collection.get('test_key')


@mock.patch('redis.Redis', mock_redis_client)
def test_redis_collections_set(collection: RedisAbstractCollection, app: Flask, entity_stub):
    redis = collection.redis
    collection.set(entity_stub)
    assert entity_stub.config == json.loads(redis.hget('test_collection', entity_stub.id))


@mock.patch('redis.Redis', mock_redis_client)
def test_redis_collection_list(collection: RedisAbstractCollection, app: Flask, entity_stub_list):
    redis = collection.redis
    for ent in entity_stub_list:
        redis.hset('test_collection', ent.id, json.dumps(ent.config))

    list = collection.list()
    assert len(list) == 10


@mock.patch('redis.Redis', mock_redis_client)
def test_redis_collections_remove(collection: RedisAbstractCollection, app: Flask, entity_stub_list):
    redis = collection.redis
    for ent in entity_stub_list:
        redis.hset('test_collection', ent.id, json.dumps(ent.config))

    assert redis.hlen('test_collection') == 10
    collection.remove('test_entity_0')
    assert redis.hlen('test_collection') == 9

