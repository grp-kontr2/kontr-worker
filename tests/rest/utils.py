import json
import logging

from flask import Response
from flask.testing import FlaskClient

log = logging.getLogger(__name__)

API_PREFIX = "/api/v1.0"


def extract_data(response: Response) -> dict:
    data = response.get_data(as_text=True)
    components = json.loads(data)
    return components


def make_request(client: FlaskClient, url: str, method: str = 'get',
                 token: str = None, data: str = None) -> Response:
    """ Creates an authenticated request to an endpoint.

    Args:
        client: Flask's test client
        url: request url
        method: get, post, put
        token(str): Access token
        data: json data to send in the request

    Returns(Response): a Werkzeug response

    """
    token = token or client.application.config.get('WORKER_SECRET')
    headers = {'content-type': 'application/json', 'Authorization': f"Bearer {token}"}
    full_url = API_PREFIX + url
    log.debug(f"[REQ] ({method}) - \"{full_url}\"")
    request_method = getattr(client, method)
    return request_method(full_url, headers=headers,
                          data=data, follow_redirects=True)
