import mock
from mockredis import mock_redis_client

from tests.rest import utils


def assert_response(response, code=200):
    assert response.status_code == code
    assert response.mimetype == 'application/json'


@mock.patch('redis.Redis', mock_redis_client)
def test_status_endpoint_should_be_accessible(client):
    response = utils.make_request(client, f"/management/status")
    assert_response(response)


@mock.patch('redis.Redis', mock_redis_client)
def test_(client):
    response = utils.make_request(client, f"/management/images")
    assert_response(response)
