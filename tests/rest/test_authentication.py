import mock
from mockredis import mock_redis_client

from tests.rest import utils


@mock.patch('redis.Redis', mock_redis_client)
def test_valid_token_will_allow_access(client):
    response = utils.make_request(client, f"/management/status")
    assert response.status_code == 200
    assert response.mimetype == 'application/json'


@mock.patch('redis.Redis', mock_redis_client)
def test_invalid_token_will_throw_exception(client):
    response = utils.make_request(client, f"/management/status", token='not_valid_token')
    assert response.status_code == 401
    assert response.mimetype == 'application/json'
    assert response.json['message'] == 'Token is not valid!'
