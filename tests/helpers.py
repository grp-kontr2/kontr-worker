import datetime
import json
import uuid

import jwt
import responses

from kontr_worker.tools import paths
from kontr_worker.app_context import AppContext
from kontr_worker.entities import Submission
from kontr_worker.execution.containers import Instance

ROOT_DIR = paths.ROOT_DIR
RESOURCES_DIR = ROOT_DIR / 'tests' / 'resources'


def get_dummy_jwt():
    expiration = datetime.datetime.utcnow() + datetime.timedelta(days=30)
    return jwt.encode({'exp': expiration}, 'test_secret').decode('utf-8')


class SubmissionMocker:
    def __init__(self, context: AppContext, submission_id=None, base_url=None):
        self.context = context
        self.submission_id = submission_id or str(uuid.uuid4())
        self.api_base = base_url or f'http://localhost/api/v1.0'

    def submission_config(self) -> dict:
        return {
            'id': self.submission_id,
            'project_id': 'test-project',
            'course_id': 'test-course',
            'test_files_hash': 'test-project-course-hash',
            'test_files_subdir': 'subdir'
            }

    def create_submission(self, config=None) -> Submission:
        config = config or self.submission_config()
        return Submission(config=config)

    def create_instance(self, submission: Submission = None, config=None) -> Instance:
        submission = submission or self.create_submission(config=config)
        return self.context.docker.get_instance(submission=submission)

    @property
    def submission_base_url(self):
        return f'{self.api_base}/submissions/{self.submission_id}'

    @property
    def worker_base_url(self):
        return f"{self.api_base}/workers/{self.context.worker.name}"

    @property
    def submission_sources(self):
        return self.submission_base_url + '/files/sources'

    @property
    def test_files(self):
        return self.submission_base_url + '/files/test_files'

    def prepare_workspace(self, instance=None) -> Instance:
        import shutil
        instance = instance or self.create_instance()
        sources_path, tests_path = self.create_archives()
        submission = instance.submission
        shutil.unpack_archive(sources_path, submission.dirs.submission_files)
        shutil.unpack_archive(tests_path, submission.dirs.test_files)
        return instance

    def create_archives(self):
        from tests.helpers import RESOURCES_DIR
        import shutil
        sources_dir = RESOURCES_DIR / 'hello'
        tests_dir = RESOURCES_DIR / 'hello-test-files'
        sources_path = shutil.make_archive(str(sources_dir), 'zip', sources_dir)
        tests_path = shutil.make_archive(str(tests_dir), 'zip', tests_dir)
        return sources_path, tests_path

    def mock_requests(self):
        sources_path, tests_path = self.create_archives()
        self.mock_submission_get()
        self.mock_worker_update()
        self.mock_login()
        self._mock_download(sources_path, self.submission_sources)
        self._mock_download(tests_path, self.test_files)
        self._mock_results()


    def mock_worker_update(self):
        responses.add(responses.PUT, self.worker_base_url, status=200,
                      content_type='application/json', body="")

    def mock_submission_get(self):
        submission_data = dict(
            id=self.submission_id,
            state='READY',
            note='',
            )
        responses.add(responses.GET, self.submission_base_url, status=200,
                      content_type='application/json', body=json.dumps(submission_data))

    def mock_login(self):
        auth_data = {'access_token': get_dummy_jwt(), 'refresh_token': get_dummy_jwt()}
        body = json.dumps(auth_data)
        responses.add(responses.POST, f"{self.api_base}/auth/login", status=200,
                      content_type='application/json', body=body)

    def _mock_download(self, fpath, url):
        with open(fpath, 'rb') as zip_file:
            responses.add(responses.GET, url, body=zip_file.read(),
                          status=200, content_type='application/zip')

    def _mock_results(self):
        responses.add(responses.POST, f"{self.submission_base_url}/files/results", status=200,
                      content_type='application/json', body="")
