import logging
import os

import pytest
from flask import Flask

from kontr_worker import create_app
from tests.helpers import SubmissionMocker

log = logging.getLogger(__name__)


@pytest.fixture(scope='function', autouse=True)
def app():
    os.environ["WORKER_CONFIG_TYPE"] = 'test'
    flask_app = create_app(environment='test')
    with flask_app.app_context():
        yield flask_app


@pytest.fixture(scope='function')
def client(app: Flask):
    with app.test_client() as _client:
        yield _client


@pytest.fixture()
def subm_mocker(app: Flask) -> SubmissionMocker:
    from kontr_worker.extensions import context
    return SubmissionMocker(context=context)
