from flask import Flask
import json
from mockredis import mock_redis_client
import pytest
import mock

from kontr_worker.collections import RedisAbstractCollection, SubmissionsCollection
from kontr_worker.entities import Submission


@pytest.fixture()
def collection():
    return SubmissionsCollection()


@pytest.fixture()
def submission_config():
    return {'id': 'test_submission', 'image_name': 'test_image'}


@pytest.fixture()
def submission(submission_config: dict) -> Submission:
    return Submission(submission_config)


def test_redis_collections_init(collection):
    assert isinstance(collection.entity_klass_instance('{}'), Submission)
    assert collection.collection_name == 'submissions'


@mock.patch('redis.Redis', mock_redis_client)
def test_redis_collections_get(collection: RedisAbstractCollection, app: Flask, submission):
    collection.set(submission)
    assert submission == collection.get(submission.id)


@mock.patch('redis.Redis', mock_redis_client)
def test_redis_collections_set(collection: RedisAbstractCollection, app: Flask, submission):
    redis = collection.redis
    collection.set(submission)
    assert submission.config == json.loads(redis.hget('submissions', submission.id))


@mock.patch('redis.Redis', mock_redis_client)
def test_redis_collections_update(collection: RedisAbstractCollection, app: Flask, submission):
    collection.set(submission)
    received = collection.get(submission.id)
    received['image_name'] = 'new_image'
    collection.set(received)
    assert len(collection.list()) == 1
    assert collection.get(submission.id)['image_name'] == 'new_image'

