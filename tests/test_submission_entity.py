import pytest

from kontr_worker.entities import Submission, SubmissionState
from kontr_worker.extensions import context


@pytest.fixture()
def config(subm_mocker) -> dict:
    return subm_mocker.submission_config()


@pytest.fixture()
def submission_instance(subm_mocker, config) -> Submission:
    return subm_mocker.create_submission(config)


def test_submission_has_correct_id(submission_instance: Submission, config: dict):
    assert submission_instance.id == config['id']


def test_submission_has_config(submission_instance: Submission, config: dict):
    assert submission_instance.config is not None
    assert submission_instance.config == config


def test_submission_has_project_id(submission_instance: Submission, config: dict):
    assert submission_instance.project_id == config['project_id']


def test_submission_has_course_id(submission_instance: Submission, config: dict):
    assert submission_instance.course_id == config['course_id']


def test_submission_has_undef_status(submission_instance: Submission, config: dict):
    assert submission_instance.status == SubmissionState.UNDEF


def test_instance_has_correct_paths(app, submission_instance, config):
    base_dir = submission_instance.dirs.base_dir
    assert base_dir == context.paths.workspace / config['id']
    assert submission_instance.dirs.test_files == base_dir / 'test_files'
    assert submission_instance.dirs.result_files == base_dir / 'result_files'
    assert submission_instance.dirs.submission_files == base_dir / 'submission_files'
