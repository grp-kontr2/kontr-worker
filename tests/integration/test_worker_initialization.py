import json

import pytest
import responses

from kontr_worker.service import ManagementService


@pytest.fixture
def mgmt(app):
    return ManagementService()


@responses.activate
def test_worker_initialization(subm_mocker, mgmt):
    subm_mocker.mock_requests()
    mgmt.register_worker()
    assert len(responses.calls) == 1
    body = json.loads(responses.calls[0].request.body)
    assert body['tags'] == mgmt.context.flask_config['WORKER_FEATURES']
    assert body['portal_secret'] == mgmt.context.flask_config['WORKER_SECRET']
    assert body['state'] == 'ready'
