import logging
from pathlib import Path

import docker
import pytest
import responses
from mock import mock
from mockredis import mock_redis_client

from kontr_worker.execution import execution
from kontr_worker.extensions import context

log = logging.getLogger(__name__)


@pytest.fixture()
def submission(subm_mocker):
    return subm_mocker.create_submission()


@pytest.fixture()
def executor(submission) -> execution.DockerExecutor:
    return execution.DockerExecutor(submission)


@pytest.fixture()
def instance(executor):
    return executor.instance


@responses.activate
def test_checkout_source_files(subm_mocker, submission, instance, executor):
    subm_mocker.mock_requests()
    executor.checkout_sources()
    files = submission.dirs.submission_files
    assert files.exists()
    assert Path(files / 'main.c').exists()
    assert not Path(str(files) + ".zip").exists()


@responses.activate
@pytest.mark.docker
def test_checkout_test_files(subm_mocker, submission, instance, executor):
    subm_mocker.mock_requests()
    executor.checkout_test_files()
    files = submission.dirs.test_files
    assert files.exists()
    assert Path(files / 'kontr_tests' / 'instructions.py').exists()
    assert Path(files / 'Pipfile').exists()
    assert Path(files / 'Dockerfile').exists()
    assert not Path(str(files) + ".zip").exists()


@responses.activate
@mock.patch('redis.Redis', mock_redis_client)
@pytest.mark.docker
def test_build_image(subm_mocker, submission, instance, executor):
    responses.add_passthru('http+docker://localhost')
    subm_mocker.prepare_workspace()
    executor.build_image()
    image_name = context.hashes.get(submission.unique_project_id)
    assert image_name == submission.test_files_id
    assert context.docker.docker.images.get(image_name)
    executor.clean_old_image()
    with pytest.raises(docker.errors.ImageNotFound):
        context.docker.docker.images.get(image_name)


@responses.activate
@mock.patch('redis.Redis', mock_redis_client)
@pytest.mark.docker
def test_run_image(subm_mocker, submission, instance, executor):
    responses.add_passthru('http+docker://localhost')
    subm_mocker.prepare_workspace()
    executor.build_image()
    container = executor.execute_container()
    assert container.status
    logs = container.logs().decode('utf-8')
    log.debug(f"[CONTAINER] Log: {logs}")
    container.remove()

    files = submission.dirs.result_files
    assert files.exists()
    generator = (Path(files).rglob("suite-stats*"))
    assert len([*generator]) == 1


@responses.activate
@mock.patch('redis.Redis', mock_redis_client)
@pytest.mark.docker
def test_executor_run(subm_mocker, submission, instance, executor):
    subm_mocker.mock_requests()
    responses.add_passthru('http+docker://localhost')
    executor.run()

    files = submission.dirs.result_files
    assert not files.exists()
    with pytest.raises(docker.errors.NotFound):
        executor.instance.container.reload()


