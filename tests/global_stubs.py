class NetworkStub(object):
    def __init__(self, *_):
        pass

    @property
    def name(self):
        return 'internal-network'


class ContainerCollectionStub:
    def __init__(self, *_):
        pass

    def create(*args, **kwargs):
        return args, kwargs

    def run(*args, **kwargs):
        return args, kwargs
