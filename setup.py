from setuptools import setup, find_packages

requirements = ['Flask', 'flask-restplus', 'marshmallow', 'gunicorn', 'requests', 'celery', 'redis',
        'docker', 'click', 'python-dotenv', 'kontr-api']
setup(name='Kontr Worker',
      version='1.0',
      description='Kontr kontr_worker',
      author='Peter Stanko',
      author_email='stanko@mail.muni.cz',
      url='https://gitlab.fi.muni.cz/grp-kontr2/kontr_worker',
      packages=find_packages(exclude=("tests",)),
      include_package_data=True,
      install_requires=requirements,
      extras_require={
          'dev': [
              'pytest',
              'coverage',
              'pytest-mock',
              'mock',
              'mockredispy',
              'responses',
              'PyJWT'
              ],
          'docs': [
              'sphinx',
              ]
          },
      )
