import logging
import json

import redis

from kontr_worker import entities

log = logging.getLogger(__name__)


class RedisAbstractCollection(object):
    """Abstraction over redis
    """

    def __init__(self, collection_name, entity_klass=None):
        """Creates instance of the collection
        Args:
            collection_name(str): Name
            entity_klass: Entity class
        """
        self.collection_name = collection_name
        self._entity_klass = entity_klass
        self._redis = None

    def entity_klass_instance(self, config, as_json=True):
        """Returns instance of the entity wrapper over dictionary
        Args:
            as_json: Whether to serialize or not
            config: Data stored in the redis
        Returns: The instance
        """
        if config is None:
            return None
        if as_json:
            config = json.loads(config)
        if self._entity_klass:
            config = self._entity_klass(config)
        log.debug(f"[COL] INSTANCE({self.collection_name}): {config}")
        return config

    @property
    def redis(self) -> redis.Redis:
        """Gets a redis instance
        Returns:

        """
        if self._redis is None:
            from kontr_worker.extensions import context
            self._redis = context.redis.create_redis()
        return self._redis

    def get(self, eid: str, as_json: bool = True):
        """Gets entity in the redis
        Args:
            as_json(bool): Whether to serialize as JSON
            eid(str): Entity id
        Returns: Entity instance
        """
        log.debug(f"[COL] GET({self.collection_name}): {eid}")
        entity_data = self.redis.hget(self.collection_name, eid)
        return self.entity_klass_instance(entity_data, as_json=as_json)

    def set(self, entity, value=None, as_json: bool = True):
        """Adds or updates entity in redis
        Args:
            value: Value to be stored
            as_json(bool): Whether to serialize as JSON
            entity: Entity instance
        Returns: Entity instance
        """
        eid = entity.id if hasattr(entity, 'id') else entity
        value = value or entity.config
        if as_json:
            value = json.dumps(value)
        log.info(f"[COL] SET({self.collection_name}): {eid}: {entity}")
        self.redis.hset(self.collection_name, eid, value)
        return entity

    def remove(self, eid: str):
        """Removes entity from redis
        Args:
            eid(str): Entity id
        Returns: Removed entity
        """
        entity = self.get(eid)
        log.info(f"[COL] REMOVE: {eid}")
        self.redis.hdel(self.collection_name, eid)
        return entity

    def list(self):
        """List of all entities
        Returns:

        """
        log.debug(f"[COL] LIST({self.collection_name})")
        items: dict = self.redis.hgetall(self.collection_name)
        return [self.entity_klass_instance(config) for (eid, config) in items.items()]


class SubmissionsCollection(RedisAbstractCollection):
    def __init__(self):
        super().__init__(
            collection_name='submissions',
            entity_klass=entities.Submission
            )

    def get_active_submissions(self):
        return self.filter_submissions(lambda submission: submission.active)

    def filter_submissions(self, predicate):
        submissions = self.list()
        return [submission for submission in submissions if predicate(submission)]


class TestFilesHashesCollection(RedisAbstractCollection):
    def __init__(self):
        super().__init__(collection_name='test-files-hashes')
