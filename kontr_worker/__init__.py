import logging
import os

from flask import Flask

from kontr_worker import async_celery, extensions, rest, tools
from kontr_worker.cli import register_commands
from kontr_worker.config import CONFIGURATIONS
from kontr_worker.logger import Logging
from kontr_worker.tools.paths import ROOT_DIR

log = logging.getLogger(__name__)


def configure_app(app: Flask, env: str = None) -> Flask:
    """Configures the Flask app based on production stage.

    Args:
        app(Flask): Flask application
        env(str): Environment for the flask application
    """
    config_type = env or os.environ.get('WORKER_ENV', 'dev')
    log.debug(f"[INIT] Portal Config Type: {config_type}")
    config_object = CONFIGURATIONS[config_type]
    if config_object is None:
        raise RuntimeError(f"There is no flask_config object for: {config_type}")
    app.config.from_object(config_object)
    app.config['WORKER_ENV'] = config_type
    log.debug("[CONFIG] Variables:")
    for (k, v) in app.config.items():
        log.debug(f"[CONFIG] {k}={v}")
    return app


def register_to_portal(app: Flask):
    from kontr_worker.service import management
    management.ManagementService().register_worker()


def _log_config(app):
    log.trace("[INIT] Loaded config: ")
    for (key, val) in app.config.items():
        log.trace(f"[CONFIG] {key}={val}")


def create_app(environment: str = None):
    """Creates and initializes the components used in the application

    Returns(Flask): Flask application instance
    """
    app = Flask(__name__)
    Logging(app).load_config()
    _log_config(app)
    # app configuration
    configure_app(app, env=environment)
    extensions.init_extensions(app)
    rest.register_namespaces(app)
    register_commands(app)
    if tools.s2b(app.config.get('WORKER_AUTOREGISTER')):
        register_to_portal(app)
    return app
