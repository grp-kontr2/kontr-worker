import logging

from kontr_worker.entities import SubmissionState
from kontr_worker.execution import execution
from kontr_worker.extensions import context

log = logging.getLogger(__name__)


@context.celery.task
def execute_submission(sid):
    submission = context.submissions.get(sid)
    log.info(f"[TASK] Execute submission ({sid}): {sid}")
    executor = execution.get_executor(submission=submission)
    executor.run()


@context.celery.task
def cancel_submission(sid: str):
    """Cancels submission
    Args:
        sid(str): Submission id
    """
    submission = context.submissions.get(sid)
    log.info(f"[TASK] Cancel submission ({sid}): {sid}")
    executor = execution.get_executor(submission=submission)
    executor.cancel_submission()
    context.docker.remove(submission=submission)
    submission.status = SubmissionState.CANCEL
    context.submissions.set(submission)


@context.celery.task
def process_result(sid: str, result: dict):
    submission = context.submissions.get(sid)
    log.info(f"[TASK] Process result ({sid}): {sid}")
    executor = execution.get_executor(submission=submission)
    executor.process_result()
