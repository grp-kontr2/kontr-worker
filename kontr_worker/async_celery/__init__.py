import celery
from flask import Flask

from kontr_worker.extensions import context


def init_app(app: Flask) -> Flask:
    """Initializes the flask application
    Args:
        app:

    Returns:

    """
    if app.config.get('BROKER_URL'):
        context.celery.conf.update(app.config)

    class ContextTask(celery.Task):
        def __call__(self, *args, **kwargs):
            with app.app_context():
                return self.run(*args, **kwargs)

    celery.Task = ContextTask
    return app
