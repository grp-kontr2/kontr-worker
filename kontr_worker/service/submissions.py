import logging

from kontr_worker import entities
from kontr_worker.app_context import AppContext
from kontr_worker.async_celery import tasks
from kontr_worker.entities import Submission, SubmissionState
from kontr_worker.extensions import context
from kontr_worker.rest import errors

log = logging.getLogger(__name__)


class SubmissionService:
    def __init__(self, sid: str = None):
        self.sid = sid

    @property
    def context(self) -> AppContext:
        return context

    @property
    def submission(self) -> Submission:
        if self.sid is None:
            return None
        return context.submissions.get(self.sid)

    def process_submission(self, data: dict):
        """Processes received submission

        Adds submission to the incoming queue
        Creates task to receive required files - test files and submission files
        Creates task to create docker image_name and runs it

        Args:
            data(dict): Submission data for kontr_worker
        """
        log.info(f"[PROC] Processing submission({self.sid}): {data}")
        submission = self._create_submission_entity(data=data)
        context.submissions.set(submission)
        self._initialize_submission()
        tasks.execute_submission.delay(self.sid)
        return submission.config

    def _initialize_submission(self):
        """Initializes the submission

        Returns:

        """
        self.submission['status'] = SubmissionState.QUEUED
        context.submissions.set(self.submission)

    def _create_submission_entity(self, data) -> entities.Submission:
        if not data['id']:
            data['id'] = self.sid
        return entities.Submission(data)

    def cancel_submission(self):
        if not self.submission.active:
            raise errors.SubmissionError(400, f"Submission {self.sid} is not active!")
        tasks.cancel_submission.delay(self.sid)
        return self.sid
