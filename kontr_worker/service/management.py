import logging

from kontr_worker.app_context import AppContext
from kontr_worker.extensions import context

log = logging.getLogger(__name__)


class ManagementService:
    @property
    def context(self) -> AppContext:
        return context

    @property
    def status(self) -> dict:
        return self.context.worker.to_dict()

    def register_worker(self):
        params = dict(
            tags=self.context.worker.features,
            portal_secret=self.context.worker.secret,
            state='ready',
        )
        log.info(f"[MGMT] Register worker {self.context.worker.name}: {params}")
        self.context.rest.workers.update(params, eid=self.context.worker.name)

    def list_images(self):
        return self.context.hashes.list()

    def clean_image(self, test_files_hash: str):
        log.debug(f"[DEL] Delete old image: {test_files_hash}")
        if context.hashes.get(test_files_hash):
            log.info(f"[DEL] Deleting old image: {test_files_hash}")
            context.hashes.remove(test_files_hash)
            context.docker.remove_image(test_files_hash)

    def clean_all_images(self):
        log.debug("[DEL] Deleting all images")
        for image in self.list_images():
            self.clean_image(image)

