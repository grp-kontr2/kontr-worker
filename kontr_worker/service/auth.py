import flask

from kontr_worker.rest import errors


class AuthValidator:
    def __init__(self):
        self.app = None

    def init_app(self, app: flask.Flask):
        self.app = app

    @property
    def config(self) -> flask.Config:
        return self.app.config

    @property
    def required_token(self):
        return self.config.get('WORKER_SECRET')

    def check_token(self, token):
        if token != self.required_token:
            raise errors.AuthorizationFailed("Token is not valid!")
        return True

    def check_request(self):
        auth: str = flask.request.headers.get('Authorization')
        if auth is None:
            raise errors.AuthorizationFailed("Authorization header is missing")
        if not auth:
            raise errors.AuthorizationFailed("Authorization token is missing")
        split = auth.split(' ')
        if split[0] != 'Bearer':
            raise errors.AuthorizationFailed("Authorization token should be bearer")
        return self.check_token(token=split[1])


def __check_token():
    from kontr_worker.extensions import context
    return context.auth_validator.check_request()
