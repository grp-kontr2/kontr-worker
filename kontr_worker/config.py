import os

from kontr_worker.tools import paths


def get_secret_token():
    import secrets
    return secrets.token_urlsafe(25)


class Config(object):
    """Base configuration
    """
    PROJECT_ROOT = paths.ROOT_DIR
    APP_DIR = paths.ROOT_DIR / 'kontr_worker'
    WORKSPACE_DIR = os.getenv('WORKSPACE_DIR', '/tmp/testing_workspace')
    MOUNT_DIR = '/workspace'
    SECRET_KEY = os.environ.get('SECRET_KEY') or 'you-will-never-guess'
    LOGGER_NAME = "flask"
    API_PREFIX = os.environ.get('WORKER_API_PREFIX', default="/api/v1.0/")
    INTERNAL_DOCKER_NETWORK = os.environ.get('INTERNAL_DOCKER_NETWORK', default='internal-network')
    WORKER_FEATURES = "docker"
    WORKER_MAX_SUBMISSIONS = 5
    WORKER_NAME = os.environ.get('WORKER_NAME', 'executor')
    WORKER_SECRET = os.getenv('WORKER_SECRET', get_secret_token())
    WORKER_AUTOREGISTER = os.environ.get('WORKER_AUTOREGISTER', False)
    REDIS_HOST = os.environ.get('WORKER_REDIS_HOST', 'localhost')
    REDIS_PORT = os.environ.get('WORKER_REDIS_PORT', 6379)
    REDIS_DB = os.environ.get('WORKER_REDIS_DB', 2)
    BROKER_URL = os.environ.get('CELERY_BROKER_URL', f'redis://{REDIS_HOST}:{REDIS_PORT}/1')
    CELERY_RESULT_BACKEND = os.environ.get('CELERY_RESULT_BACKEND', BROKER_URL)
    CELERY_DEFAULT_QUEUE = f'kontr_worker-{WORKER_NAME}'
    PORTAL_URL = os.environ.get('PORTAL_URL', 'http://localhost:8000')
    PORTAL_SECRET = os.environ.get('PORTAL_SECRET', 'test_token')
    WORKER_CLEAN_SUBMISSION_FILES = os.environ.get('WORKER_CLEAN_SUBMISSION_FILES',
                                                   'True')
    WORKER_CLEAN_SUBMISSION_CONTAINER = os.environ.get('WORKER_CLEAN_SUBMISSION_CONTAINER',
                                                       'True')


class DevelopmentConfig(Config):
    PORTAL_SECRET = os.environ.get('PORTAL_SECRET', 'executor_secret')
    WORKER_SECRET = os.environ.get('WORKER_SECRET', '53PnmDqOKDcwExKp4Ol1QQREZB7Sk_IOcg')
    WORKER_AUTOREGISTER = os.environ.get('WORKER_AUTOREGISTER', True)


class TestConfig(Config):
    PORTAL_URL = 'http://localhost'
    PORTAL_SECRET = 'test_token'
    WORKER_SECRET = 'secret-testing-token'
    WORKSPACE_DIR = '/tmp/testing_workspace'
    WORKER_AUTOREGISTER = False


class ProductionConfig(Config):
    pass


CONFIGURATIONS = dict(
    dev=DevelopmentConfig(),
    test=TestConfig(),
    production=ProductionConfig()
)
