from pathlib import Path

import flask

from kontr_worker.collections import SubmissionsCollection, TestFilesHashesCollection


def _to_dict(obj, allowed):
    params = {}
    for name in allowed:
        params[name] = getattr(obj, name)
    return params


class WorkerContext:
    def __init__(self, context: 'AppContext'):
        self.context = context

    @property
    def config(self) -> flask.Config:
        return self.context.flask_config

    @property
    def name(self) -> str:
        return self.config.get('WORKER_NAME')

    @property
    def secret(self) -> str:
        return self.config.get('WORKER_SECRET')

    @property
    def features(self) -> str:
        return self.config.get('WORKER_FEATURES')

    @property
    def max_submissions(self) -> int:
        return self.config.get('WORKER_MAX_SUBMISSIONS')

    @property
    def active_submissions(self) -> int:
        return len(self.context.submissions.get_active_submissions())

    def to_dict(self):
        return _to_dict(self, ['name', 'features', 'max_submissions', 'active_submissions'])


class ContextPaths:
    def __init__(self, context: 'AppContext'):
        self._context = context

    @property
    def workspace(self) -> Path:
        return Path(self._context.flask_config.get('WORKSPACE_DIR'))


class AppContext:
    def __init__(self):
        self._auth_validator = None
        self._app = None
        from kontr_worker import tools, execution
        import celery
        self.docker = execution.DockerWrapper()
        self.redis = tools.RedisClient()
        self.celery = celery.Celery()
        self.portal = tools.PortalClient()
        self._submissions = SubmissionsCollection()
        self._hashes = TestFilesHashesCollection()
        self._worker = WorkerContext(self)

    @property
    def worker(self) -> WorkerContext:
        return self._worker

    @property
    def paths(self) -> ContextPaths:
        return ContextPaths(context=self)

    @property
    def auth_validator(self):
        if self._auth_validator is None:
            from kontr_worker.service import auth
            self._auth_validator = auth.AuthValidator()
        return self._auth_validator

    @property
    def submissions(self) -> SubmissionsCollection:
        return self._submissions

    def init_app(self, app: flask.Flask):
        """Initializes the AppContext using the flask app
        Args:
            app(flask.Flask): Flask app instance
        """
        self._app = app
        self.__init_subcomponents()

    def __init_subcomponents(self):
        """Initializes subcomponents
        """
        from kontr_worker import async_celery
        self.docker.init_app(self._app)
        self.redis.init_app(self._app)
        async_celery.init_app(self._app)
        self.portal.init_app(self._app)
        self.auth_validator.init_app(self.app)

    @property
    def app(self) -> flask.Flask:
        """Gets flask application instance
        Returns(flask.Flask): Flask application instance
        """
        return self._app

    @property
    def flask_config(self) -> flask.Config:
        return self.app.config

    @property
    def rest(self):
        return self.portal.client

    @property
    def hashes(self) -> TestFilesHashesCollection:
        return self._hashes
