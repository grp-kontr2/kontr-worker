"""
Logging configuration module
"""
import logging
from logging.config import dictConfig

import flask

from kontr_worker.tools import paths

"""
Logging configuration module
"""


class Logging:
    def __init__(self, app: flask.Flask = None):
        self._app = app

    @property
    def global_log_level(self):
        if not self._app:
            return 'INFO'
        return self._app.config.get('LOG_LEVEL_GLOBAL', 'INFO')

    @property
    def file_log_level(self):
        if not self._app:
            return 'INFO'
        return self._app.config.get('LOG_LEVEL_FILE', self.global_log_level)

    @property
    def console_log_level(self):
        if not self._app:
            return 'INFO'
        return self._app.config.get('LOG_LEVEL_CONSOLE', self.global_log_level)

    @property
    def handlers(self):
        return {
            'console': self.get_handler_console(),
            'worker_file': self.get_logger_file('kontr_worker'),
            'flask_file': self.get_logger_file('flask'),
            'api_file': self.get_logger_file('kontr_api'),
        }

    @property
    def formatters(self):
        return {
            'verbose': {'format': '%(asctime)s - %(name)s - %(levelname)s - %(message)s'},
            'simple': {'format': '%(levelname)s %(message)s'},
            'colored_console': {
                '()': 'coloredlogs.ColoredFormatter',
                'format': "%(asctime)s - %(name)s - %(levelname)s - %(message)s",
                'datefmt': '%H:%M:%S'
            },
        }

    @property
    def loggers(self) -> dict:
        return {
            'kontr_worker': {
                'handlers': ['console', 'worker_file'],
                'level': self.global_log_level, 'propagate': True
            },
            'tests': {
                'handlers': ['console'], 'level': self.global_log_level, 'propagate': True
            },
            'app': {
                'handlers': ['console'], 'level': self.global_log_level, 'propagate': True
            },
            'flask': {
                'handlers': ['console', 'flask_file'], 'level': self.global_log_level,
                'propagate': True
            },
            'kontr_api': {
                'handlers': ['console', 'api_file'], 'level': self.global_log_level, 'propagate': True
            },
            'werkzeug': {
                'handlers': ['console'], 'level': self.global_log_level, 'propagate': True
            },
            'docker': {
                'handlers': ['console'], 'level': self.global_log_level, 'propagate': True
            },
        }

    @property
    def config(self):
        return {
            'version': 1,
            'disable_existing_loggers': True,
            'formatters': self.formatters,
            'handlers': self.handlers,
            'loggers': self.loggers,
        }

    def get_logger_file(self, name, level: str = None):
        level = level or self.file_log_level
        return {
            'level': level,
            'class': 'logging.handlers.RotatingFileHandler',
            'formatter': 'verbose',
            'filename': str(paths.LOG_DIR / f'{name}.log'),
            'maxBytes': 5000000,  # 5MB
            'backupCount': 5
        }

    def get_handler_console(self, level: str = None):
        level = level or self.console_log_level
        return {
            'level': level, 'class': 'logging.StreamHandler', 'formatter': 'colored_console'
        }

    def load_config(self):
        """Loads config based on the config type
        Args:
        """
        add_custom_log_level()
        dictConfig(self.config)


TRACE_LOG_LVL = 9


def _trace(self, message, *args, **kws):
    if self.isEnabledFor(TRACE_LOG_LVL):
        # Yes, logger takes its '*args' as 'args'.
        self._log(TRACE_LOG_LVL, message, args, **kws)


def add_custom_log_level():
    logging.addLevelName(TRACE_LOG_LVL, 'TRACE')
    logging.Logger.trace = _trace

