import flask

from kontr_worker.app_context import AppContext
from kontr_worker.cli.errors import WorkerCliException
from kontr_worker.entities import Submission
from kontr_worker.execution import execution
from kontr_worker.service import ManagementService, SubmissionService


class CommandsManager(object):
    def __init__(self):
        self._context = None
        self._mgmt = ManagementService()

    def get_sub_executor(self, sid: str):
        submission = self.get_submission(sid)
        return execution.get_executor(submission=submission), submission

    def get_submission(self, sid: str) -> Submission:
        submission = self.context.submissions.get(sid)
        if not submission:
            raise WorkerCliException(f"Cannot find submission with id: {sid}")
        return submission

    @property
    def context(self) -> AppContext:
        from kontr_worker.extensions import context
        if self._context is None:
            self._context = context
        return self._context

    @property
    def app(self) -> flask.Flask:
        return self.context.app

    @property
    def mgmt(self) -> ManagementService:
        return self._mgmt

    def list_submissions(self, predicate):
        for submission in self.context.submissions.filter_submissions(predicate=predicate):
            print(f"{submission.id}: {submission}")

    def cancel_submission(self, submission_id: str):
        executor, submission = self.get_sub_executor(submission_id)
        executor.cancel_submission()
        print(f"Submission \"{submission.id}\" has been cancelled: {submission}")

    def create_submission(self, sid: str, params: dict):
        SubmissionService(sid).process_submission(params)
        print(f"Running the submission ({sid}) with params: {params}")

    def status(self):
        status = self.mgmt.status
        print("Status: \n")

        for (key, val) in status.items():
            print(f"{key}: {val}")
        return status

    def register_worker(self):
        return self.mgmt.register_worker()

    def clean_image(self, test_files_id):
        ManagementService().clean_image(test_files_id)

    def clean_all_images(self):
        ManagementService().clean_all_images()

