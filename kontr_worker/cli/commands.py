from typing import List

import click
from flask.cli import AppGroup

from kontr_worker.cli.manager import CommandsManager

submissions_cli = AppGroup('submissions', help='Submissions management')
management_cli = AppGroup('mgmt', help='General management')

commands = CommandsManager()


@submissions_cli.command('list-processing', help="List processing submission")
def submissions_cli_list_processing():
    print("List: ")
    commands.list_submissions(lambda s: s.active())


@submissions_cli.command('create', help="Create submission")
@click.option('-p', '--param', multiple=True, help="Parameter for the submission")
@click.argument('sid')
def submissions_cli_create(sid, param):
    params = parse_param(param)
    print(f"Creating submission: {sid} with params: {params}")
    commands.create_submission(sid, params)


@submissions_cli.command('cancel', help="Cancel the submission")
@click.argument('sid')
def submissions_cli_cancel(sid):
    print(f"Canceling submission: {sid}")
    commands.cancel_submission(sid)


@management_cli.command('register', help="Register worker")
def management_cli_register():
    print(f"Register worker")
    commands.register_worker()


@management_cli.command('status', help="Print worker status")
def submissions_cli_status():
    commands.status()


@management_cli.command('clean', help="Clean image cache")
@click.argument('test_files_id')
def submissions_cli_status(test_files_id):
    commands.clean_image(test_files_id)


@management_cli.command('clean-all', help="Clean image cache")
def submissions_cli_status():
    commands.clean_all_images()


def parse_param(array: List[str]) -> dict:
    params = {}
    for item in array:
        split = item.split(":")
        if len(split) == 2:
            params[split[0]] = split[1]
    return params
