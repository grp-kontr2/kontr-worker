import flask

from .commands import management_cli, submissions_cli


def register_commands(app: flask.Flask) -> flask.Flask:
    app.cli.add_command(management_cli)
    app.cli.add_command(submissions_cli)
    return app
