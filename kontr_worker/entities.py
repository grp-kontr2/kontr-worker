"""
Entities module
"""
from enum import Enum
from pathlib import Path

from kontr_api import resources


class SubmissionState:
    ACTIVE = 'active'
    QUEUED = 'queued'
    DONE = 'done'
    CANCEL = 'cancel'
    UNDEF = 'NOT_DEFINED'


class SubmissionDirCollection:
    def __init__(self, base):
        self._base_dir = Path(base)

    @property
    def base_dir(self):
        return self._base_dir

    @property
    def test_files(self) -> Path:
        return self.base_dir / 'test_files'

    @property
    def submission_files(self) -> Path:
        return self.base_dir / 'submission_files'

    @property
    def result_files(self) -> Path:
        return self.base_dir / 'result_files'


class Submission:
    def __init__(self, config: dict):
        """Creates instance of the Submission
        Args:
            config(dict): Configuration dictionary
        """
        self._config: dict = config
        self._resource = None
        self._dirs = None

    @property
    def dirs(self) -> SubmissionDirCollection:
        if self._dirs is None:
            self._dirs = SubmissionDirCollection(self.base_dir)
        return self._dirs

    @property
    def base_dir(self):
        from kontr_worker.extensions import context
        return context.paths.workspace / str(self.id)

    @property
    def resource(self) -> resources.Submission:
        """Gets resource instance from the portal
        Returns(resources.Submission): Submission Resource
        """
        if self._resource is None:
            from kontr_worker.extensions import context
            self._resource = context.portal.client.submissions.read(self.id)
        return self._resource

    @property
    def id(self) -> str:
        """Submission id
        Returns(str): Submission id
        """
        return self.config['id']

    @property
    def config(self) -> dict:
        """Gets configuration dictionary
        Returns(dict): Config dictionary

        """
        return self._config

    @property
    def container_id(self):
        return self.config.get('container_id')

    @property
    def project_id(self) -> str:
        return self.config.get('project_id')

    @property
    def unique_project_id(self):
        return f"{self.course_id}-{self.project_id}"

    @property
    def course_id(self) -> str:
        return self.config.get('course_id')

    @property
    def test_files_hash(self):
        return self.config.get('test_files_hash')

    @property
    def test_files_subdir(self):
        return self.config.get('test_files_subdir')

    @property
    def test_files_id(self):
        test_id = self.test_files_hash
        if self.test_files_subdir:
            test_id += "_" + self.test_files_subdir
        return test_id

    @property
    def active(self) -> bool:
        return self.status == SubmissionState.ACTIVE

    def done(self) -> bool:
        return self.status == SubmissionState.DONE

    def __getitem__(self, item):
        return self.config[item]

    def __setitem__(self, key, value):
        self.config[key] = value

    @property
    def status(self) -> str:
        return self.config.get('status') or SubmissionState.UNDEF

    @status.setter
    def status(self, value):
        self.config['status'] = value

    def __str__(self) -> str:
        return str(self.config)

    def __repr__(self):
        return str(self.config)

    def __eq__(self, other):
        return self.id == other.id
