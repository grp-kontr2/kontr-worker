import abc
import logging
from pathlib import Path
from shutil import rmtree
from time import sleep

from docker.models.containers import Container
from kontr_api import resources

from kontr_worker import entities, tools
from kontr_worker.entities import SubmissionState
from kontr_worker.extensions import context
from kontr_worker.tools import fs_tools

log = logging.getLogger(__name__)


def _extract_files(file_path: Path, location: Path):
    log.debug(f"[EXTRACT] Extracting files: {file_path} -> {location}")
    fs_tools.unzip_files(file_path, location)
    file_path.unlink()


class AbstractExecutor:
    def __init__(self, submission):
        self._submission = submission

    @property
    def submission(self) -> entities.Submission:
        return self._submission

    @property
    def resource(self) -> resources.Submission:
        return self.submission.resource

    def upload_results(self) -> Path:
        sid = self.submission.id
        log.info(f"[UPLOAD] Uploading result: {sid}")
        zipped = fs_tools.zip_files(sid, self.submission.dirs.result_files)
        return self.resource.results.upload(zipped)

    def checkout_sources(self):
        location: Path = self.submission.dirs.submission_files
        location.mkdir(parents=True)
        file_path = location / 'sources.zip'
        log.debug(f"[CHECKOUT] Checking out the sources to \"{file_path}\"")
        self.resource.sources.download(file_path)
        _extract_files(file_path, location)

    def checkout_test_files(self):
        location: Path = self.submission.dirs.test_files
        location.mkdir(parents=True)
        file_path = location / 'test_files.zip'
        log.debug(f"[CHECKOUT] Checking out the test files to \"{file_path}\"")
        self.resource.test_files.download(file_path)
        _extract_files(file_path, location)

    def checkout_files(self):
        log.debug(f"[CHECKOUT] Checking out the files: {self.submission.id}")
        self.checkout_sources()
        self.process_test_files()

    @abc.abstractmethod
    def process_test_files(self):
        pass

    def process_result(self):
        self.submission.status = SubmissionState.DONE
        context.submissions.set(self.submission)
        self.upload_results()


class DockerExecutor(AbstractExecutor):
    def __init__(self, submission):
        super().__init__(submission)
        self._instance = None

    @property
    def instance(self):
        if self._instance is None:
            self._instance = context.docker.get_instance(submission=self.submission)
        return self._instance

    def build_image(self):
        test_files_dir = self.submission.dirs.test_files
        tag = self.submission.test_files_id

        log.info(f"[DOCKER] Building the ({tag}): {test_files_dir}")
        path = str(test_files_dir)
        image, _ = context.docker.build_image(path=path, tag=tag, rm=True)
        log.debug(f" -> Built Image ({image.tags}): {image}")

        context.hashes.set(self.submission.unique_project_id, tag)
        return image

    def clean_old_image(self):
        old_image_name = context.hashes.get(self.submission.unique_project_id)
        if old_image_name:
            log.info(f"[DOCKER] Deleting old image: {old_image_name}")
            context.docker.remove_image(old_image_name)

    def process_test_files(self):
        test_files_id = context.hashes.get(self.submission.unique_project_id)

        if test_files_id is not None and test_files_id == self.submission.test_files_id:
            log.debug("[SKIP] Processing not required - image already exists")
            if context.docker.get_image(test_files_id) is not None:
                log.debug("[SKIP] Image exists in the registry")
                return
            else:
                log.warning("Image does not exists in the registry - needs a clean up")

        log.debug(f"Updating the image to [{self.submission.test_files_id}]")
        self.checkout_test_files()
        self.clean_old_image()
        self.build_image()

    def execute_container(self) -> Container:
        container = self.instance.run()
        self.submission['container_id'] = self.instance.container.id
        self.submission['status'] = SubmissionState.ACTIVE
        context.submissions.set(self.submission)
        log.info(f"[STATS] Container stats for {self.submission.id}: "
                 f"{container.stats(stream=False)}")
        return container

    def run(self):
        self.checkout_files()
        container = self.execute_container()
        self.wait_for_container_to_stop(container)
        self.process_result()
        self.clean_up()

    def clean_up(self):
        log.info(f"[CLEAN] Cleaning up the submission: {self.submission}")
        if tools.s2b(context.app.config.get('WORKER_CLEAN_SUBMISSION_FILES')):
            rmtree(self.submission.base_dir)

        context.submissions.remove(self.submission.id)

        if tools.s2b(context.app.config.get('WORKER_CLEAN_SUBMISSION_CONTAINER')):
            context.docker.remove(self.submission)

    def cancel_submission(self):
        self.clean_up()

    def wait_for_container_to_stop(self, container: Container):
        max_wait = 50
        container.wait()
        log.info(f"Waiting for the container: {container.name} - {container.status}")
        while True:
            status = container.status
            if status == 'exited':
                break
            if max_wait <= 0:
                log.error(f"[WAIT] Container {container.name} "
                          f"did not exited in time: {container.status}")
                break
            container.reload()
            max_wait -= 1
            sleep(2)
        log.info(f"[CONTAINER] Logs for {container.name}: {container.logs().decode('utf-8')}")


def get_executor(submission) -> DockerExecutor:
    return DockerExecutor(submission=submission)
