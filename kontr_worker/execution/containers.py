"""
Containers module
"""
import logging
import os
from pathlib import Path
from typing import Optional

import docker
import flask
from docker.errors import APIError, ImageNotFound
from docker.models.containers import Container
from docker.models.images import Image

from kontr_worker.collections import TestFilesHashesCollection
from kontr_worker.entities import Submission, SubmissionDirCollection

log = logging.getLogger(__name__)


class DockerWrapper:
    """Wraps Docker and uses the Flask Config
    """

    def __init__(self, app: flask.Flask = None):
        """Creates instance of the docker wrapper
        Args:
            app(flask.Flask): Flask application
        """
        self._app = app
        self._docker = None

    def init_app(self, app: flask.Flask):
        """Initializes the application
        Args:
            app(flask.Flask): Flask application
        """
        self._app = app

    @property
    def docker(self) -> docker.DockerClient:
        """Gets instance of the docker
        Returns(DockerClient): Instance of the Docker client

        """
        if self._docker is None:
            self._docker = docker.from_env(version='auto')
        return self._docker

    @property
    def app(self) -> flask.Flask:
        """Gets instance of the Flask application
        Returns(flask.Flask): Flask application instance
        """
        return self._app

    @property
    def config(self) -> flask.Config:
        """Flask flask_config instance
        Returns(flask.Config): Flask flask_config instance

        """
        return self.app.config

    @property
    def workspace_dir(self) -> Path:
        """Gets workspace dir path
        Returns(Path): Workspace directory
        """
        return Path(self.config['WORKSPACE_DIR'])

    @property
    def mount_dir(self) -> Path:
        """Gets mount dir path
        Returns(Path): Mount path

        """
        return Path(self.config['MOUNT_DIR'])

    def get_instance(self, submission: Submission) -> 'Instance':
        """Creates instance of the custom docker container wrapper
        Args:
            submission(Submission): Submission instance

        Returns(Instance):

        """
        return Instance(self, submission=submission)

    def get_container(self, submission: Submission) -> Container:
        return self.docker.containers.get(submission.container_id)

    def get_image(self, name) -> Optional[Image]:
        """Gets a docker image instance from the registry
        Args:
            name(str): Name of the image

        Returns(Optional[Image]): Docker image or None
        """
        try:
            return self.docker.images.get(name)
        except ImageNotFound as ex:
            log.warning(f"Docker image \"{name}\" not found in the registry: {ex}")
            return None

    def remove_image(self, name, **kwargs):
        try:
            self.docker.images.remove(name, **kwargs)
        except APIError as ex:
            log.error(f"[RMI] Cannot remove the image \"{name}\": {ex}")

    def build_image(self, **kwargs):
        try:
            return self.docker.images.build(**kwargs)
        except APIError as ex:
            log.error(f"[RMI] Cannot build the image: {ex}")
            return None

    def stop(self, submission: Submission):
        """Stops the container
        Args:
            submission:

        Returns:

        """
        container = self.get_container(submission)
        container.stop()
        return container

    def remove(self, submission: Submission):
        container = self.get_container(submission)
        if container.status == 'running':
            container.stop()
        container.remove()
        return container


class Instance(object):
    def __init__(self, wrapper: DockerWrapper, submission: Submission):
        """Creates instance of the docker container
        Args:
            wrapper(DockerWrapper): Docker wrapper
            submission(Submission): Submission instance
        """
        self.submission = submission
        self.wrapper = wrapper
        self.container = self.__get_container()

    @property
    def hashes(self) -> TestFilesHashesCollection:
        from kontr_worker.extensions import context
        return context.hashes

    @property
    def mounts(self) -> SubmissionDirCollection:
        return SubmissionDirCollection(self.wrapper.mount_dir)

    def create(self, **kwargs) -> Container:
        # https://docker-py.readthedocs.io/en/stable/containers.html
        defaults = self._default_runtime_params or {}
        params = {**defaults, **kwargs}
        log.info(f"[DOCKER] Creating container with params: {params}")
        self.container = self.docker.containers.create(**params)
        return self.container

    def run(self, **kwargs) -> Container:
        if self.container is None:
            self.container = self.create(**kwargs)
        log.info(f"[DOCKER] Running container with params: {kwargs}")
        self.container.start(**kwargs)
        self.container.reload()
        return self.container

    @property
    def docker(self) -> docker.DockerClient:
        """Gets instance of the docker client
        Returns(docker.DockerClient): Instance of the docker client
        """
        return self.wrapper.docker

    def _create_volumes(self) -> dict:
        return {
            **self.__submission_files_volume,
            **self.__result_files_volume
        }

    @property
    def image_name(self):
        return self.hashes.get(self.submission.unique_project_id)

    @property
    def __submission_files_volume(self) -> dict:
        return self.__prepare_volume('submission_files')

    @property
    def __result_files_volume(self) -> dict:
        return self.__prepare_volume('result_files', 'rw')

    @property
    def _default_runtime_params(self) -> Optional[dict]:
        if self.image_name is None:
            return None
        params = dict(
            image=self.image_name,
            name=f"kontr-submission-{self.submission.id}",
            network=None,
            volumes=self._create_volumes(),
            environment=self.__create_env_vars(),
            user=os.getuid(),
        )
        return params

    def __prepare_volume(self, name: str, mode: str = 'ro') -> dict:
        path: str = self.get_host_dir(name)
        path_path = Path(path)
        if not path_path.exists():
            path_path.mkdir(parents=True)
        return {
            str(path): {
                'bind': self.get_mount_dir(name),
                'mode': mode
            }
        }

    def get_mount_dir(self, name):
        return str(getattr(self.mounts, name))

    def get_host_dir(self, name):
        return str(getattr(self.submission.dirs, name))

    def __create_env_vars(self) -> dict:
        return dict(
            KTDK_WORKSPACE='/tmp',
            KTDK_TEST_FILES=self.get_mount_dir('test_files'),
            KTDK_SUBMISSION=self.get_mount_dir('submission_files'),
            KTDK_RESULTS=self.get_mount_dir('result_files')
        )

    def __get_container(self):
        container_id = self.submission.container_id
        if container_id is not None:
            return self.wrapper.get_container(submission=self.submission)
        return None
