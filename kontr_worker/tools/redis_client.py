from flask import Flask
import redis


class RedisClient(object):
    def __init__(self):
        """Creates instance of the Redis Client
        """
        self._pool = None
        self._app = None

    def init_app(self, app: Flask):
        """Initializes the application
        Args:
            app(Flask): Flask instance
        """
        self._app = app

    @property
    def app(self) -> Flask:
        """Gets instance of the Flask application
        Returns(Flask): Flask application
        """
        return self._app

    @property
    def redis_host(self) -> str:
        """Returns redis host name, default is 'localhost'
        Returns(str): Name of the host
        """
        return self.app.config.get('REDIS_HOST', 'localhost')

    @property
    def redis_port(self) -> int:
        """Returns the redis port
        Returns(int): Redis port number
        """
        return int(self.app.config.get('REDIS_PORT', 6379))

    @property
    def redis_db(self) -> int:
        """Gets redis db number
        Returns(int): Redis db number
        """
        return int(self.app.config.get('REDIS_DB', 1))

    @property
    def pool(self) -> redis.ConnectionPool:
        """Gets Redis connection pool instance
        Returns(redis.ConnectionPool): Connection pool instance
        """
        if self._pool is None:
            self._pool = redis.ConnectionPool(
                host=self.redis_host, port=self.redis_port, db=self.redis_db
                )
        return self._pool

    def create_redis(self) -> redis.Redis:
        """Creates instance of the redis instance
        Returns:

        """
        return redis.Redis(connection_pool=self.pool)

