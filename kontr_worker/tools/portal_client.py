from flask import Flask
from kontr_api import KontrClient


class PortalClient:
    def __init__(self):
        """Creates instance of the portal client
        """
        self._app = None
        self._client = None

    def init_app(self, app: Flask):
        """Initializes the application
        Args:
            app(Flask): Flask instance
        """
        self._app = app

    @property
    def app(self) -> Flask:
        """Gets app instance
        Returns(Flask): Flask app instance
        """
        return self._app

    @property
    def url(self) -> str:
        """Gets portal url
        Returns(str): Portal url
        """
        return self.app.config.get('PORTAL_URL')

    @property
    def secret(self) -> str:
        """Gets secret
        Returns(str): Kontr client secret
        """
        return self.app.config.get('PORTAL_SECRET')

    @property
    def worker_name(self) -> str:
        """Gets worker name
        Returns(str): Worker name
        """
        return self.app.config.get('WORKER_NAME')

    @property
    def client(self) -> KontrClient:
        """Gets Kontr client instance
        Returns(KontrClient): Kontr client instance
        """
        if self._client is None:
            self._client = KontrClient(url=self.url,
                                       secret=self.secret,
                                       identifier=self.worker_name)
        return self._client
