from .portal_client import PortalClient
from .redis_client import RedisClient


def s2b(bstr: str) -> bool:
    if bstr is None:
        return False
    if isinstance(bstr, bool):
        return bstr
    if isinstance(bstr, str):
        return bstr.lower() in ['true', 't', '1', 'y', 'yes']
    return False
