from pathlib import Path
import shutil


def zip_files(name, result_files: Path) -> Path:
    return Path(shutil.make_archive(f"{ result_files / '..' / name}", 'zip', str(result_files)))


def unzip_files(zip_file: Path, to_dir: Path):
    shutil.unpack_archive(str(zip_file), str(to_dir), 'zip')
