from os.path import dirname, abspath
from pathlib import Path

ROOT_DIR = Path(dirname(dirname(dirname(abspath(__file__)))))
LOG_DIR = ROOT_DIR / 'log'
