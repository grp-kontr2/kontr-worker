import functools

from kontr_worker.service.auth import __check_token


def require_authorization(method):
    @functools.wraps(method)
    def __auth(*args, **kwargs):
        __check_token()
        return method(*args, **kwargs)
    return __auth
