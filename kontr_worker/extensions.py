import flask

from kontr_worker.app_context import AppContext

context: AppContext = AppContext()


def init_extensions(app: flask.Flask) -> flask.Flask:
    context.init_app(app)
    return app
