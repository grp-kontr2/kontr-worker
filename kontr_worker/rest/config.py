import logging
from flask_restplus import Namespace, Resource

management_namespace = Namespace('management')
log = logging.getLogger(__name__)


@management_namespace.route('')
class ConfigResource(Resource):
    def get(self):
        """Receives submission params as JSON
        Returns:
        """
        return {'status': 'it works'}

    def post(self):
        """Receives submission params as JSON
        Returns:
        """
        return {'status': 'it works'}
