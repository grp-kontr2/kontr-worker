import logging

from flask_restplus import Namespace, Resource

from kontr_worker.service.management import ManagementService
from kontr_worker.tools.decorators import require_authorization

management_namespace = Namespace('management')
log = logging.getLogger(__name__)


@management_namespace.route('/status')
class StatusResource(Resource):
    @require_authorization
    def get(self):
        """Receives submission params as JSON
        Returns:
        """
        return ManagementService().status


@management_namespace.route('/images')
class ImagesResource(Resource):
    @require_authorization
    def get(self):
        return ManagementService().list_images()


@management_namespace.route('/images/<string:img>/clean')
class ImagesResource(Resource):
    @require_authorization
    def post(self, img):
        return ManagementService().clean_image(img)


@management_namespace.route('/registration')
class RegisterResource(Resource):
    @require_authorization
    def post(self):
        return ManagementService().register_worker()
