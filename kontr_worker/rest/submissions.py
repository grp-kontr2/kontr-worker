import logging

import flask
from flask_restplus import Namespace, Resource

from kontr_worker.extensions import context
from kontr_worker.rest import helpers
from kontr_worker.service.submissions import SubmissionService
from kontr_worker.tools.decorators import require_authorization

submissions_namespace = Namespace('submissions')
result_namespace = Namespace('result')
log = logging.getLogger(__name__)


@submissions_namespace.route('')
class SubmissionsResource(Resource):
    @require_authorization
    def get(self):
        """Get submissions list
        Returns:
        """
        return context.submissions.list()


@submissions_namespace.route('/<string:sid>')
class SubmissionResource(Resource):
    @require_authorization
    def get(self, sid: str):
        """Get submission
        Args:
            sid(str): Submission id
        Returns:
        """
        submission = context.submissions.get(sid)
        return flask.jsonify(submission.config)

    @require_authorization
    def post(self, sid: str):
        """Receives submission params as JSON
        Attributes:
            sid(str): Submission id
        """
        data = helpers.require_data('create', 'submission')
        service = SubmissionService(sid)
        result = service.process_submission(data)
        return result

    @require_authorization
    def delete(self, sid: str):
        """Receives submission params as JSON
        Attributes:
            sid(str): Submission id
        """
        service = SubmissionService(sid)
        result = service.cancel_submission()
        return {"submission_id": result}
