"""
Rest layer module
"""

from flask import Flask
from flask_restplus import Api


API_PREFIX = "/api/v1.0"


def create_api():
    return Api(
        title='Kontr Portal API',
        version='1.0',
        description='Kontr Portal API',
        doc=f"{API_PREFIX}/docs",
        prefix=API_PREFIX
    )


rest_api = create_api()


def register_namespaces(app: Flask):
    """Registers namespaces for the application
    Args:
       app(Flask): Flask application
    Returns(Flask): Flask application
    """

    from .management import management_namespace
    rest_api.add_namespace(management_namespace)

    from .submissions import submissions_namespace, result_namespace
    rest_api.add_namespace(submissions_namespace)
    rest_api.add_namespace(result_namespace)
    rest_api.init_app(app)
    register_error_handlers(app)
    return app


def register_error_handlers(app: Flask):
    from kontr_worker.rest import errors
    errors.load_errors(app)
