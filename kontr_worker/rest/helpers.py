"""
Helpers to work and process the rest requests and responses
"""

from flask import request

from kontr_worker.rest.errors import DataMissingError


def parse_request_data(schema, action: str, resource: str) -> dict:
    """Parses the request data using schema
    Args:
        schema(Schema): Resource schema
        action(str): Action that is used
        resource(str): Name of the resource

    Returns(dict): Parsed dictionary
    """
    json_data = require_data(action, resource=resource)
    return schema.load(json_data)[0]


def require_data(action: str, resource: str) -> dict:
    """Parses data from json and if they are missing, throws an exception
    Args:
        action(str): Name of the action
        resource(str): Name of the resource

    Returns(dict): Parsed json data

    """
    json_data = request.get_json()
    if not json_data:
        raise DataMissingError(action=action, resource=resource)
    return json_data
