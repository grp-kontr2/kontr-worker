import logging

import flask
from flask import Flask
from flask_restplus import abort

from kontr_worker.rest import rest_api

log = logging.getLogger(__name__)


def load_errors(app: Flask):
    log.debug("[LOAD] Custom error handlers loaded")
    for (ex, func) in rest_api.error_handlers.items():
        app.register_error_handler(ex, func)


@rest_api.errorhandler
def default_error_handler():
    return flask.jsonify({'message': 'Default error handler has been triggered'}), 400


class WorkerError(Exception):
    def __init__(self, message=None):
        self.message = message


class WorkerApiError(WorkerError):
    def __init__(self, code, message=None):
        super().__init__(message=message)
        self.code = code


class DataMissingError(WorkerApiError):
    pass


class SubmissionError(WorkerApiError):
    def __init__(self, code, message):
        super(SubmissionError, self).__init__(message=message, code=code)


class AuthorizationFailed(WorkerApiError):
    def __init__(self, message):
        super().__init__(message=message, code=401)


@rest_api.errorhandler(WorkerError)
def handle_general_worker_error(ex: WorkerError):
    log.error(f"[WORKER] Worker error: {ex} ")
    abort(code=400, message=ex.message)


@rest_api.errorhandler(WorkerApiError)
def handle_general_worker_api_error(ex: WorkerError):
    log.error(f"[API] Api error: {ex} ")
    abort(code=ex.code, message=ex.message)
