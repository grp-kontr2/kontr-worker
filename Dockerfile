FROM kontr2/base-pipenv

ADD . /app
RUN chmod 777 -R /app
WORKDIR /app
EXPOSE 5000

ONBUILD COPY Pipfile Pipfile
ONBUILD COPY Pipfile.lock Pipfile.lock

RUN pipenv install --system

CMD ["gunicorn", "--bind", "0.0.0.0:5000", "--workers", "3", "app:app"]


