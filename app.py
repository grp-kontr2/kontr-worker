import logging

from dotenv import load_dotenv

from kontr_worker import create_app

load_dotenv(verbose=True)


def get_celery():
    from kontr_worker.extensions import context
    return context.celery


log = logging.getLogger(__name__)
app = create_app()
celery = get_celery()

if __name__ == '__main__':
    pass
